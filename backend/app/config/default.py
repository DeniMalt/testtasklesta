from os import environ

from pydantic_settings import BaseSettings


class DefaultSettings(BaseSettings):
    """
    Default configs for application.

    Usually, we have three environments: for development, testing and production.
    But in this situation, we only have standard settings for local development.
    """
    ENV: str = environ.get("ENV", "local")
    PATH_PREFIX: str = "/api/v1"
    APP_HOST: str = "http://127.0.0.1"
    APP_PORT: int = 8000

    # to get a string like this run: "openssl rand -hex 32"

    LOG_FILE: str = "operations.log"
