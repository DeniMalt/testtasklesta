from .ping import api_router as ping_route
from .calculation_tf_idf import api_router as tf_idf_route


list_of_routes = [
    ping_route,
    tf_idf_route
]


__all__ = [
    "list_of_routes"
]
