from fastapi import APIRouter, Depends, HTTPException, Request,  UploadFile, File
from starlette import status


from app.utils.calculation import calc_tf_idf


api_router = APIRouter(
    prefix="",
    tags=["Calculation"],
)


@api_router.post(
    "/tf_idf",
    status_code=status.HTTP_200_OK,
)
async def calculation_idf_tf(
    _: Request,
    file: UploadFile = File(...)
):
    contents = file.file.read()
    with open(file.filename, 'wb') as f:
        f.write(contents)
    result = await calc_tf_idf(file.filename)
    return result
