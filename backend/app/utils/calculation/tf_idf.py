import re
import os
import math


async def calc_tf_idf(
    filename: str
) -> list[dict]:
    absolute_path = os.path.abspath(filename)
    counts_words = {}
    count_str_included_some_word = {}
    result = []
    with open(absolute_path, 'r') as file:
        lines = file.readlines()
        for line in lines:
            separated_line = re.findall(r'\w+', line)
            for word in separated_line:
                if word in counts_words:
                    counts_words[word] += 1
                else:
                    counts_words[word] = 1
            set_separated_line = list(set(separated_line))
            for word in set_separated_line:
                if word in count_str_included_some_word:
                    count_str_included_some_word[word] += 1
                else:
                    count_str_included_some_word[word] = 1
        for elem in counts_words:
            idf = math.log(count_str_included_some_word[elem] / counts_words[elem])
            result.append({"word": elem, "tf": counts_words[elem], "idf": idf})
        result.sort(key=lambda x: x["idf"], reverse=True)
    return result[:50]
